package main

import (
	"context"
	"crypto/md5"
	"errors"
	"fmt"
	pb "gitlab.com/pdragnev/math-web-service/gen/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"io"
	"log"
	"net"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"sync"
)

type Stats struct {
	lock *sync.RWMutex
	data map[string]ErrorValue
}

func (s *Stats) Get(key string) (*ErrorValue, bool) {
	s.lock.RLock()
	defer s.lock.RUnlock()
	d, ok := s.data[key]
	return &d, ok
}

func (s *Stats) Set(key string, d *ErrorValue) {
	defer s.lock.Unlock()

	if val, ok := s.Get(key); ok {
		s.lock.Lock()
		val.Frequency += 1
		s.data[key] = *val
	} else {
		s.lock.Lock()
		s.data[key] = *d
	}
}

func (s *Stats) GetDataMap() *map[string]ErrorValue {
	return &s.data
}

type ErrorValue struct {
	Expression string `json:"expression"`
	Endpoint   string `json:"endpoint"`
	Frequency  int    `json:"frequency"`
	Type       string `json:"type"`
}

func (k *ErrorValue) HashKey() string {
	h := md5.New()
	io.WriteString(h, k.Expression)
	io.WriteString(h, k.Endpoint)
	io.WriteString(h, k.Endpoint)
	return string(h.Sum(nil))
}

var stats Stats

type Operation string

const (
	Plus       Operation = "plus"
	Minus                = "minus"
	Multiplied           = "multiplied"
	Divided              = "divided"
	Squared              = "squared"
	Remainder            = "remainder"
	Cubed                = "cubed"
	Power                = "power"
)

func (s Operation) Contains() bool {
	switch s {
	case Plus, Minus, Multiplied, Divided, Squared, Remainder, Cubed, Power:
		return true
	}
	return false
}

func (s Operation) isAllowedOperations() bool {
	switch s {
	case Plus, Minus, Multiplied, Divided:
		return true
	}
	return false
}

func (s Operation) isDivideOperation() bool {
	if s == Divided {
		return true
	}
	return false
}
func (s Operation) isMultipliesOperation() bool {
	if s == Multiplied {
		return true
	}
	return false
}

const INVALID_SYNTAX_ERROR = "Invalid syntax."
const NON_MATH_ERROR = "Non-math questions."
const UNSUPPORTED_OPR_ERROR = "Unsupported operations."

type server struct {
	pb.UnimplementedMathApiServer
}

func init() {
	stats = Stats{
		lock: new(sync.RWMutex),
		data: make(map[string]ErrorValue),
	}
}

func (*server) Evaluate(ctx context.Context, req *pb.MathRequest) (*pb.MathResponse, error) {
	sliceResult, err := validateExpression(strings.Split(strings.Trim(req.Expression, "!?. "), " "))

	if err != nil {
		errMsg := err.Error()
		updateStats(req.Expression, "/evaluate", errMsg)
		return nil, status.Errorf(codes.InvalidArgument, errMsg)
	}

	return &pb.MathResponse{Result: int32(calculateExpression(sliceResult))}, nil

}

func (*server) Validate(ctx context.Context, req *pb.MathRequest) (*pb.ValidateResponse, error) {
	_, err := validateExpression(strings.Split(strings.Trim(req.Expression, "!?."), " "))

	if err != nil {
		errMsg := err.Error()

		updateStats(req.Expression, "/validate", errMsg)

		return &pb.ValidateResponse{
			Valid:  "false",
			Reason: &errMsg,
		}, nil
	}

	return &pb.ValidateResponse{
		Valid: "true",
	}, nil
}

func (*server) Errors(ctx context.Context, req *pb.ErrorsRequest) (*pb.ErrorsResponse, error) {
	var elemSlice []*pb.ErrorsResponseBody

	for _, value := range *stats.GetDataMap() {
		v := pb.ErrorsResponseBody{
			Expression: value.Expression,
			Endpoint:   value.Endpoint,
			Frequency:  int32(value.Frequency),
			Type:       value.Type,
		}
		elemSlice = append(elemSlice, &v)
	}
	return &pb.ErrorsResponse{Errors: elemSlice}, nil
}

func validateExpression(sliceExpression []string) ([]string, error) {
	lookingForNumber := true
	isExpressionFound := false
	var sliceResult []string

	for i := 0; i < len(sliceExpression); i++ {
		switch lookingForNumber {
		case true:
			if _, err := strconv.Atoi(sliceExpression[i]); err == nil {
				sliceResult = append(sliceResult, sliceExpression[i])
				lookingForNumber = false
				isExpressionFound = true
			} else if isExpressionFound {
				return nil, errors.New(INVALID_SYNTAX_ERROR)
			}
		case false:
			if r := Operation(sliceExpression[i]); r.Contains() {
				if !r.isAllowedOperations() {
					return nil, errors.New(UNSUPPORTED_OPR_ERROR)
				}
				sliceResult = append(sliceResult, sliceExpression[i])
				if r.isDivideOperation() || r.isMultipliesOperation() {
					if i < len(sliceExpression)-1 && sliceExpression[i+1] == "by" {
						i++
					} else {
						return nil, errors.New(INVALID_SYNTAX_ERROR)
					}
				}
				lookingForNumber = true
				continue
			}
			return nil, errors.New(INVALID_SYNTAX_ERROR)
		}
	}

	if len(sliceResult) == 0 {
		return nil, errors.New(NON_MATH_ERROR)
	}
	if len(sliceResult)%2 == 0 {
		return nil, errors.New(INVALID_SYNTAX_ERROR)
	}
	return sliceResult, nil
}

func calculateExpression(sliceResult []string) int {
	result, _ := strconv.Atoi(sliceResult[0])
	for i := 1; i < len(sliceResult); i++ {
		if i+1 < len(sliceResult) {
			n, _ := strconv.Atoi(sliceResult[i+1])
			switch Operation(sliceResult[i]) {
			case Plus:
				result += n
			case Minus:
				result -= n
			case Divided:
				result /= n
			case Multiplied:
				result *= n
			}
		}

	}
	return result
}

func updateStats(exp string, endpoint string, errMsg string) {
	errorValue := ErrorValue{
		Expression: exp,
		Endpoint:   endpoint,
		Frequency:  1,
		Type:       errMsg,
	}
	stats.Set(errorValue.HashKey(), &errorValue)
}

func main() {
	listener, err := net.Listen("tcp", "localhost:8080")
	if err != nil {
		log.Fatalln(err)
	}
	grpcServer := grpc.NewServer()

	pb.RegisterMathApiServer(grpcServer, &server{})

	go func() {
		fmt.Println("Starting server...")
		if err := grpcServer.Serve(listener); err != nil {
			log.Fatalf("Failed to serve: %v", err)
		}
	}()

	//Wait for Control C to exit
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)

	//Block until signal
	<-ch
	fmt.Println("Stopping server")
	grpcServer.Stop()
	fmt.Println("Stopping listener")
	listener.Close()
}
