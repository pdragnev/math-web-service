package main

import (
	"context"
	pb "gitlab.com/pdragnev/math-web-service/gen/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/test/bufconn"
	"log"
	"net"
	"testing"
)

const bufSize = 1024 * 1024

var lis *bufconn.Listener

func init() {
	lis = bufconn.Listen(bufSize)
	s := grpc.NewServer()
	pb.RegisterMathApiServer(s, &server{})
	go func() {
		if err := s.Serve(lis); err != nil {
			log.Fatalf("Server exited with error: %v", err)
		}
	}()
}

func bufDialer(context.Context, string) (net.Conn, error) {
	return lis.Dial()
}

func TestServer_Evaluate(t *testing.T) {
	ctx := context.Background()
	conn, err := grpc.DialContext(ctx, "bufnet", grpc.WithContextDialer(bufDialer), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		t.Fatalf("Failed to dial bufnet: %v", err)
	}
	defer conn.Close()
	client := pb.NewMathApiClient(conn)
	resp, err := client.Evaluate(ctx, &pb.MathRequest{Expression: "What is 5 plus 5"})
	if err != nil {
		t.Fatalf("Evaluate failed: %v", err)
	}
	log.Printf("Response: %+v", resp)
	// Test for output here.
}

func TestServer_Validate(t *testing.T) {
	ctx := context.Background()
	conn, err := grpc.DialContext(ctx, "bufnet", grpc.WithContextDialer(bufDialer), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		t.Fatalf("Failed to dial bufnet: %v", err)
	}
	defer conn.Close()
	client := pb.NewMathApiClient(conn)
	resp, err := client.Evaluate(ctx, &pb.MathRequest{Expression: "What is 5 plus 5"})
	if err != nil {
		t.Fatalf("Evaluate failed: %v", err)
	}
	log.Printf("Response: %+v", resp)
	// Test for output here.
}

func TestServer_Errors(t *testing.T) {
	ctx := context.Background()
	conn, err := grpc.DialContext(ctx, "bufnet", grpc.WithContextDialer(bufDialer), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		t.Fatalf("Failed to dial bufnet: %v", err)
	}
	defer conn.Close()
	client := pb.NewMathApiClient(conn)
	resp, err := client.Evaluate(ctx, &pb.MathRequest{Expression: "What is 5 plus 5"})
	if err != nil {
		t.Fatalf("Evaluate failed: %v", err)
	}
	log.Printf("Response: %+v", resp)
	// Test for output here.
}
