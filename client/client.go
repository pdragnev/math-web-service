package main

import (
	"context"
	"encoding/json"
	"github.com/gorilla/mux"
	pb "gitlab.com/pdragnev/math-web-service/gen/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"io"
	"log"
	"net/http"
)

var client pb.MathApiClient

type Expression struct {
	Expression string `json:"expression"`
}

type ErrorResp struct {
	ErrorCode int    `json:"errorCode"`
	ErrorDesc string `json:"errorDesc"`
}

func evaluateExpression(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	expression, err := parseRequestExpression(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	resp, err := client.Evaluate(context.Background(), &pb.MathRequest{Expression: expression.Expression})
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		errResp := ErrorResp{
			ErrorCode: 400,
			ErrorDesc: err.Error(),
		}
		jsonError, jsonErr := json.Marshal(errResp)
		if jsonErr != nil {
			log.Fatalf("Unable to encode JSON")
		}
		w.Write(jsonError)
		return
	}
	json.NewEncoder(w).Encode(resp)
}

func validateExpression(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	expression, err := parseRequestExpression(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	resp, _ := client.Validate(context.Background(), &pb.MathRequest{Expression: expression.Expression})

	json.NewEncoder(w).Encode(resp)
}

func getErrors(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	resp, _ := client.Errors(context.Background(), &pb.ErrorsRequest{})

	json.NewEncoder(w).Encode(resp)
}

func parseRequestExpression(body io.ReadCloser) (Expression, error) {
	dec := json.NewDecoder(body)
	dec.DisallowUnknownFields()
	var expression Expression
	err := dec.Decode(&expression)
	return expression, err
}

func main() {
	r := mux.NewRouter()

	r.HandleFunc("/evaluate", evaluateExpression).Methods("POST")
	r.HandleFunc("/validate", validateExpression).Methods("POST")
	r.HandleFunc("/errors", getErrors).Methods("GET")

	conn, err := grpc.Dial("localhost:8080", grpc.WithTransportCredentials(insecure.NewCredentials()))
	defer conn.Close()

	if err != nil {
		log.Println(err)
	}

	client = pb.NewMathApiClient(conn)

	// Start server
	log.Fatal(http.ListenAndServe(":8000", r))
}
