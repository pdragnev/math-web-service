# Math word problems web service

A web service that will evaluate simple math word problems, with the following endpoints:

##### POST /evaluate

##### POST /validate

##### GET /errors

## Installation

To install go to a folder where you want to set up the project and clone the repo

```bash
git clone https://gitlab.com:pdragnev/math-web-service.git
```

Open to cloned directory and in a terminal run

```go
go get
go build
```

This will generate the binary for the app

## Usage
Run this command in the main directory of the project
```bash
##to start the server on port 8080
 go run .\server\server.go

##to start the http client on port 8000
go run .\client\client.go
```

